package com.alex.api

import com.alex.api.networking.NetworkSingle

interface GithubApi {

  /**
   * Returns the top 100 repos with the top committer.
   */
  fun fetchTopStarredRepos(): NetworkSingle<List<Repo>>
}