package com.alex.api

data class GithubApiConfig(
  val url: String,
  val apiToken: String,
)