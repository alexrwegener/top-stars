package com.alex.api

data class Repo(
  val owner: String,
  val repo: String,
  val ownerAvatarUrl: String,
  val topCommitter: String?
)