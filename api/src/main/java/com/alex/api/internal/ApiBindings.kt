package com.alex.api.internal

import com.alex.api.GithubApi
import com.alex.api.internal.repos.RealGithubApi
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
internal abstract class ApiBindings {

  @Binds
  abstract fun bindGithubApi(realGithubApi: RealGithubApi): GithubApi
}