package com.alex.api.internal

import com.alex.api.GithubApiConfig
import com.alex.api.internal.networking.SingleCallAdapterFactory
import com.alex.api.internal.repos.RepositoriesService
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal class ApiModule {

  @Provides
  @Singleton
  internal fun providesRepositoriesApi(
    client: OkHttpClient,
    moshi: Moshi,
    config: GithubApiConfig,
  ): RepositoriesService {
    return createRepositoriesApi(config = config, moshi = moshi, client = client)
  }

  companion object {
    internal fun createRepositoriesApi(
      config: GithubApiConfig,
      scheduler: Scheduler = Schedulers.io(),
      client: OkHttpClient,
      moshi: Moshi,
    ): RepositoriesService {
      val clientBuilder = client.newBuilder()

      if (config.apiToken.isNotEmpty()) {
        // Add api token if it exists
        clientBuilder.addNetworkInterceptor { chain ->
          val request = chain.request()
            .newBuilder()
            .addHeader("Authorization", "token ${config.apiToken}")
            .build()
          chain.proceed(request)
        }
      }

      clientBuilder.addNetworkInterceptor { chain ->
        // Specify api version + json format
        val request = chain.request()
          .newBuilder()
          .addHeader("Accept", "application/vnd.github.v3+json")
          .build()
        chain.proceed(request)
      }

      return Retrofit.Builder()
        .client(clientBuilder.build())
        .baseUrl(config.url)
        .addCallAdapterFactory(SingleCallAdapterFactory(subscribeOnScheduler = scheduler))
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create()
    }
  }
}