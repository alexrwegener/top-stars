package com.alex.api.internal.networking

import com.alex.api.networking.Either
import com.alex.api.networking.NetworkError
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.exceptions.Exceptions
import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

internal class SingleCallAdapter<R>(
  private val responseType: Type,
  private val subscribeOnScheduler: Scheduler,
) : CallAdapter<R, Any> {

  override fun responseType(): Type {
    return responseType
  }

  // This is a nice abstraction point to inject observability, metrics, logging
  override fun adapt(call: Call<R>): Single<Any> {
    return Single.create { emitter: SingleEmitter<Either<Any?, Any>> ->
      // Since call is a one-shot type, clone it for each new observer.
      val currentCall = call.clone()
      emitter.setCancellable { currentCall.cancel() }

      try {
        val response = currentCall.execute()
        val body = response.body()
        val success = if (body != null) {
          Either.success(body)
        } else {
          Either.failure(NetworkError)
        }
        emitter.onSuccess(success)
      } catch (throwable: Throwable) {
        Exceptions.throwIfFatal(throwable)
        val failure = Either.failure(NetworkError)
        emitter.onSuccess(failure)
      }
    }.subscribeOn(subscribeOnScheduler)
      .cast(Any::class.java)
  }
}