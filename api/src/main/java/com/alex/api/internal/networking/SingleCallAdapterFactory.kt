package com.alex.api.internal.networking

import com.alex.api.networking.Either
import com.alex.api.networking.NetworkError
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * A [CallAdapter.Factory] which uses RxJava 3 for creating Singles.
 *
 * ```
 * interface MyService {
 *   fun getUser(): Single<Either<User, NetworkError>>
 * }
 * ```
 * or
 * ```
 * interface MyService {
 *   fun getUser(): NetworkSingle<User>
 * }
 * ```
 *
 * A call adapter factory is super helpful to inject observability + analytics + logging for all
 * network interactions.
 */
class SingleCallAdapterFactory(
  private val subscribeOnScheduler: Scheduler,
) : CallAdapter.Factory() {

  override fun get(
    returnType: Type,
    annotations: Array<Annotation>,
    retrofit: Retrofit
  ): CallAdapter<*, *> {
    val rawType = getRawType(returnType)
    check(rawType == Single::class.java) {
      "Single must be the return type was: ${rawType.simpleName}."
    }
    check(returnType is ParameterizedType) {
      "Single must be parameterized."
    }
    val singleType = getParameterUpperBound(0, returnType)
    val rawSingleType = getRawType(singleType)
    check(rawSingleType == Either::class.java) {
      "Either must be the parameter of Single was: ${rawSingleType.simpleName}."
    }
    check(singleType is ParameterizedType) {
      "Either must be parameterized."
    }
    val errorType = getParameterUpperBound(1, singleType)
    val rawErrorType = getRawType(errorType)
    val isNetworkError = rawErrorType == NetworkError::class.java
    check(isNetworkError) {
      "NetworkError must be the second parameter of Either was: ${rawErrorType.simpleName}."
    }
    return SingleCallAdapter<Any>(
      responseType = getParameterUpperBound(0, singleType),
      subscribeOnScheduler = subscribeOnScheduler,
    )
  }
}