package com.alex.api.internal.repos

import com.alex.api.GithubApi
import com.alex.api.Repo
import com.alex.api.networking.Either
import com.alex.api.networking.NetworkError
import com.alex.api.networking.NetworkSingle
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

internal class RealGithubApi @Inject constructor(
  private val service: RepositoriesService
) : GithubApi {
  override fun fetchTopStarredRepos(): NetworkSingle<List<Repo>> {
    return service.fetchMostStarred().enrichWithTopContributors()
  }

  private fun NetworkSingle<RepositoryResponse>.enrichWithTopContributors(): NetworkSingle<List<Repo>> {
    return flatMap { repoResponse ->
      when (repoResponse) {
        is Either.Success -> {
          repoResponse.success.items.forEachRepoFetchTopCommitter().map { Either.success(it) }
        }
        is Either.Failure -> {
          Single.just(Either.failure(NetworkError))
        }
      }
    }
  }

  private fun List<Repository>.forEachRepoFetchTopCommitter(): Single<List<Repo>> {
    return Single.concat(map { repo ->
      fetchTopContributorForRepo(repo = repo)
    }).toList()
  }

  private fun fetchTopContributorForRepo(repo: Repository): Single<Repo> {
    val owner = repo.owner.login
    val repoName = repo.name
    return service.fetchTopContributor(
      owner = owner,
      repo = repoName
    ).map { contributorResponse ->
      // If we cared about the status of api failures for top contributors we could propagate it here
      val topCommitter = when (contributorResponse) {
        is Either.Success -> {
          // Grab the first contributor as the list is sorted
          contributorResponse.success.firstOrNull()?.login
        }
        else -> {
          null
        }
      }
      Repo(
        owner = owner,
        ownerAvatarUrl = repo.owner.avatar_url,
        repo = repoName,
        topCommitter = topCommitter,
      )
    }
  }
}