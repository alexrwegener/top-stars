package com.alex.api.internal.repos

import com.alex.api.networking.NetworkSingle
import com.squareup.moshi.JsonClass
import retrofit2.http.GET
import retrofit2.http.Path

internal interface RepositoriesService {
  @GET("search/repositories?q=stars:>0&sort=stars&order=desc&page=1&per_page=100")
  fun fetchMostStarred(): NetworkSingle<RepositoryResponse>

  @GET("repos/{owner}/{repo}/contributors?page=1&per_page=1")
  fun fetchTopContributor(
    @Path("owner") owner: String,
    @Path("repo") repo: String
  ): NetworkSingle<List<Contributor>>
}

@JsonClass(generateAdapter = true)
internal data class RepositoryResponse(
  val items: List<Repository>,
)

@JsonClass(generateAdapter = true)
internal data class Repository(
  val name: String,
  val owner: Owner,
)

@JsonClass(generateAdapter = true)
internal data class Owner(
  val login: String,
  val avatar_url: String,
)

@JsonClass(generateAdapter = true)
internal data class Contributor(
  val login: String
)