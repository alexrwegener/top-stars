package com.alex.api.networking

/**
 * Union type containing success or failure.
 */
sealed class Either<out S, out F> {
  data class Success<S, F> internal constructor(val success: S) : Either<S, F>()
  data class Failure<S, F> internal constructor(val failure: F) : Either<S, F>()

  /**
   * Creation methods in place to expose a subclass of Either
   * as it's parent. This is done for generic friendliness
   * where types are captured as `? super T` - such as
   * most map or flatMap implementations
   */
  companion object {
    fun <T> success(success: T): Either<T, Nothing> {
      return Success(success)
    }

    fun <T> failure(failure: T): Either<Nothing, T> {
      return Failure(failure)
    }
  }
}
