package com.alex.api.networking

/**
 * Our UI does not differentiate between error use cases (eg no internet or different API errors
 * from github like rate limiting). If it did, this class could be converted to a sealed class
 * representing the minimum set of errors applicable to all network calls.
 */
object NetworkError