package com.alex.api.networking

import io.reactivex.rxjava3.core.Single
import com.alex.api.internal.networking.SingleCallAdapterFactory

/**
 * Typealias to simplify defining a type accepted by [SingleCallAdapterFactory]
 */
typealias NetworkSingle<T> = Single<Either<T, NetworkError>>