package com.alex.api

import com.alex.api.internal.ApiModule
import com.alex.api.internal.repos.RealGithubApi
import com.alex.api.networking.Either
import com.alex.api.networking.NetworkError
import com.google.common.truth.Truth.assertThat
import com.squareup.moshi.Moshi
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import junit.framework.Assert.fail
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test

/**
 * There's a number of tests that should be written here that I didn't include due to time.
 *
 * With MockWebServer it'd be awesome to validate the api contract so that the interaction is
 * explicit. (do we provide the proper github api version header, token etc). Ideally we'd have a
 * custom dispatcher that provides canned responses when the proper api is hit vs enqueueing.
 */
class RealGithubApiTest {

  @get:Rule
  val server = MockWebServer()
  private val url = server.url("/").toString()

  @Test
  fun `can fetch top starred repos`() {
    val owner = "someOrg"
    val repo = "someRepo"
    val avatar = "someUrl"
    server.enqueue(
      MockResponse().setBody(
        createReposBody(
          repo = repo,
          owner = owner
        )
      ).setResponseCode(200)
    )
    server.enqueue(
      MockResponse().setBody(createTopCommitter("James")).setResponseCode(200)
    )
    server.enqueue(
      MockResponse().setBody(createTopCommitter("Worthy")).setResponseCode(200)
    )

    val api = createGithubApi()
    when (val response = api.fetchTopStarredRepos().testForValue()) {
      is Either.Failure -> {
        fail()
      }
      is Either.Success -> {
        val repos = response.success
        assertThat(repos).hasSize(3)
        assertThat(repos[0]).isEqualTo(
          Repo(
            owner = owner,
            repo = repo,
            ownerAvatarUrl = "$owner-url",
            topCommitter = "James"
          )
        )
        assertThat(repos[1]).isEqualTo(
          Repo(
            owner = "$owner-2",
            repo = "$repo-2",
            ownerAvatarUrl = "$owner-url-2",
            topCommitter = "Worthy"
          )
        )
        assertThat(repos[2]).isEqualTo(
          Repo(
            owner = "$owner-3",
            repo = "$repo-3",
            ownerAvatarUrl = "$owner-url-3",
            // Simulate failing an API call.
            topCommitter = null
          )
        )
      }
    }
  }

  @Test
  fun `return error on failed network call`() {
    server.enqueue(
      MockResponse().setResponseCode(422)
    )

    val api = createGithubApi()
    when (val response = api.fetchTopStarredRepos().testForValue()) {
      is Either.Failure -> {
        assertThat(response.failure).isEqualTo(NetworkError)
      }
      is Either.Success -> {
        fail()
      }
    }
  }

  private fun createGithubApi(): GithubApi {
    val config = GithubApiConfig(url = url, "githubApiToken")
    val api = ApiModule.createRepositoriesApi(
      config = config,
      scheduler = Schedulers.trampoline(),
      // In a larger app you wouldn't want to do this for lots of tests as it's slow.
      // Instead use https://github.com/square/okhttp/blob/master/okhttp-testing-support/src/main/kotlin/okhttp3/OkHttpClientTestRule.kt
      client = OkHttpClient(),
      moshi = Moshi.Builder().build(),
    )
    return RealGithubApi(api)
  }

  private fun createReposBody(
    owner: String,
    repo: String
  ): String {
    return """
    |{
    |  "items": [
    |    {
    |      "name": "$repo",
    |      "owner": {
    |        "login": "$owner",
    |        "avatar_url": "$owner-url"
    |      }
    |    }, 
    |    {
    |      "name": "$repo-2",
    |      "owner": {
    |        "login": "$owner-2",
    |        "avatar_url": "$owner-url-2"
    |      }
    |    },
    |    {
    |      "name": "$repo-3",
    |      "owner": {
    |        "login": "$owner-3",
    |        "avatar_url": "$owner-url-3"
    |      }
    |    }
    |  ]
    |}
    """.trimMargin()
  }

  private fun createTopCommitter(committer: String): String {
    return """
    |[{
    |  "login": "$committer"
    |},
    |{
    |  "login": "ignore"
    |}]
    """.trimMargin()
  }

  private fun <T : Any> Single<T>.testForValue(): T {
    return test().assertNoErrors().assertComplete().assertValueCount(1).values()[0]
  }
}