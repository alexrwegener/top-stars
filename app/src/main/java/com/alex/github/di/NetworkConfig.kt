package com.alex.github.di

import android.app.Application
import com.alex.api.GithubApiConfig
import com.alex.github.BuildConfig
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import java.io.File
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkConfig {
  @Provides
  fun provideGithubApiToken(): GithubApiConfig {
    return GithubApiConfig(
      url = "https://api.github.com/",
      apiToken = BuildConfig.GITHUB_API_KEY,
    )
  }

  @Provides
  @Singleton
  fun provideRootOkHttpClient(app: Application): OkHttpClient {
    val builder = OkHttpClient.Builder()
    builder.cache(
      Cache(
        File(app.cacheDir, "http_cache"),
        50L * 1024L * 1024L, // 50 MiB
      )
    )
    return builder.build()
  }

  @Provides
  @Singleton
  fun provideMoshi(): Moshi {
    return Moshi.Builder().build()
  }
}