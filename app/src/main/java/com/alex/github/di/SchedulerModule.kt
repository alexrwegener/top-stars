package com.alex.github.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class SchedulerModule {
  @Singleton
  @Provides
  fun provideSchedulers(): SchedulerProvider {
    return object : SchedulerProvider {
      override fun io(): Scheduler {
        return Schedulers.io()
      }

      override fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
      }
    }
  }
}