package com.alex.github.ui.repos

sealed class TopStarsIntent {
  object Load : TopStarsIntent()
  object PullToRefresh : TopStarsIntent()
  object DismissError : TopStarsIntent()
}