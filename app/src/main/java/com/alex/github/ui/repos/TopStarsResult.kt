package com.alex.github.ui.repos

import com.alex.api.Repo

sealed class TopStarsResult {
  data class Success(val repos: List<Repo>): TopStarsResult()
  object Failure : TopStarsResult()
  object InFlight : TopStarsResult()
  object DismissMessage : TopStarsResult()
}