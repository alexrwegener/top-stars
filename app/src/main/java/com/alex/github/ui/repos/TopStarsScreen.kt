package com.alex.github.ui.repos

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rxjava3.subscribeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.alex.api.Repo
import com.alex.github.R
import com.alex.github.ui.theme.GithubTheme
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@Composable
fun TopStarsScreen(
  viewModel: TopStarsViewModel = viewModel()
) {
  val state = viewModel.state().subscribeAsState(initial = TopStarsState.EMPTY)
  LaunchedEffect(Unit) {
    viewModel.init()
  }
  val scaffoldState = rememberScaffoldState()

  state.value.message?.let { msg ->
    val message = stringResource(id = msg)
    LaunchedEffect(message) {
      scaffoldState.snackbarHostState.showSnackbar(
        message = message,
        duration = SnackbarDuration.Long
      )
      viewModel.dismissSnackbar()
    }
  }
  Scaffold(
    scaffoldState = scaffoldState,
    topBar = {
      TopAppBar(
        backgroundColor = MaterialTheme.colors.primary,
        title = { Text(stringResource(R.string.app_name)) }
      )
    },
    snackbarHost = { snackbarHostState ->
      SwipeDismissSnackbarHost(
        hostState = snackbarHostState,
        modifier = Modifier
          .padding(horizontal = 16.dp)
          .fillMaxWidth()
      )
    },
    modifier = Modifier.fillMaxHeight()
  ) {
    SwipeRefresh(
      state = rememberSwipeRefreshState(isRefreshing = state.value.refreshing),
      onRefresh = { viewModel.pullToRefresh() },
    ) {
      if (state.value.repos.isEmpty()) {
        Column(
          modifier = Modifier.fillMaxSize(),
          verticalArrangement = Arrangement.Center,
          horizontalAlignment = Alignment.CenterHorizontally
        ) {
          Text(
            text = "Pretend this is a shimmer background that looks like the list of cards.",
            modifier = Modifier.padding(horizontal = 16.dp)
          )
        }
      } else {
        RepoList(state.value.repos)
      }
    }
  }
}

@Composable
private fun RepoList(repos: List<Repo>) {
  LazyColumn(
    modifier = Modifier.fillMaxWidth(),
    contentPadding = PaddingValues(16.dp)
  ) {
    items(repos) { repo ->
      RepoCard(repo)
    }
  }
}

@Composable
private fun RepoCard(repo: Repo) {
  Card(
    modifier = Modifier
      .fillMaxWidth()
      .wrapContentHeight()
      .padding(vertical = 4.dp),
    shape = MaterialTheme.shapes.medium,
    elevation = 4.dp,
    backgroundColor = MaterialTheme.colors.surface
  ) {
    val padding = 16.dp
    Row(
      verticalAlignment = Alignment.CenterVertically
    ) {
      Image(
        painter = rememberImagePainter(
          data = repo.ownerAvatarUrl,
          builder = {
            transformations(CircleCropTransformation())
          }
        ),
        contentDescription = "avatar",
        modifier = Modifier
          .padding(padding)
          .size(64.dp),
        contentScale = ContentScale.Fit
      )
      Column(
        modifier = Modifier
          .padding(vertical = 8.dp)
          .fillMaxWidth()
      ) {
        Text(
          text = "${repo.owner}/${repo.repo}",
          style = MaterialTheme.typography.h6,
          modifier = Modifier
            .padding(bottom = 8.dp)
            .fillMaxWidth(),
        )
        repo.topCommitter?.let {
          Text(text = "Top Committer: ${repo.topCommitter}")
        }
      }
    }
  }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
  val repo = Repo(
    owner = "alex",
    repo = "github",
    ownerAvatarUrl = "https://avatars.githubusercontent.com/u/3000285?v=4",
    topCommitter = "me"
  )
  GithubTheme {
    Surface(color = MaterialTheme.colors.background) {
      RepoList(listOf(repo, repo, repo))
    }
  }
}
