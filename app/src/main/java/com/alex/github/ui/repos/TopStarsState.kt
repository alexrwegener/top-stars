package com.alex.github.ui.repos

import com.alex.api.Repo

data class TopStarsState(
  val repos: List<Repo> = listOf(),
  val refreshing: Boolean = false,
  // Notification that the core API call failed
  val message: Int? = null,
) {

  companion object {
    val EMPTY = TopStarsState()
  }
}