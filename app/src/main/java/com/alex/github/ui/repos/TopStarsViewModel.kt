package com.alex.github.ui.repos

import androidx.lifecycle.ViewModel
import com.alex.api.GithubApi
import com.alex.api.networking.Either
import com.alex.github.R
import com.alex.github.di.SchedulerProvider
import com.jakewharton.rx3.replayingShare
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableTransformer
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.subjects.PublishSubject
import javax.inject.Inject

@HiltViewModel
class TopStarsViewModel @Inject constructor(
  private val api: GithubApi,
  private val schedulers: SchedulerProvider,
) : ViewModel() {
  // Represents the intent of the user for any screen action.
  private val intents: PublishSubject<TopStarsIntent> = PublishSubject.create()
  private val states: Observable<TopStarsState> = compose()

  fun state(): Observable<TopStarsState> = states

  fun init() {
    intents.onNext(TopStarsIntent.Load)
  }

  fun pullToRefresh() {
    intents.onNext(TopStarsIntent.PullToRefresh)
  }

  fun dismissSnackbar() {
    intents.onNext(TopStarsIntent.DismissError)
  }

  private fun intentProcessor(): ObservableTransformer<TopStarsIntent, TopStarsResult> {
    return ObservableTransformer<TopStarsIntent, TopStarsResult> { intents ->
      intents.flatMap { intent ->
        when (intent) {
          TopStarsIntent.DismissError -> {
            Observable.just(TopStarsResult.DismissMessage)
          }
          else -> {
            // Convert Load and PullToRefresh to a Result.InFlight followed by success or failure.
            api.fetchTopStarredRepos()
              // Allow for multiple downstream emissions
              .map { response ->
                val result: TopStarsResult = when (response) {
                  is Either.Failure -> {
                    TopStarsResult.Failure
                  }
                  is Either.Success -> {
                    TopStarsResult.Success(repos = response.success)
                  }
                }
                result
              }
              .toObservable()
              .observeOn(schedulers.ui())
              .startWithItem(TopStarsResult.InFlight)
          }
        }
      }
    }
  }

  private fun reducer(): BiFunction<TopStarsState, TopStarsResult, TopStarsState> {
    return BiFunction { previous: TopStarsState, result: TopStarsResult ->
      val state = when (result) {
        TopStarsResult.InFlight -> {
          TopStarsState(
            repos = previous.repos,
            refreshing = true,
          )
        }
        TopStarsResult.Failure -> {
          TopStarsState(
            repos = previous.repos,
            message = R.string.top_repo_failure
          )
        }
        is TopStarsResult.Success -> {
          TopStarsState(
            repos = result.repos,
          )
        }
        TopStarsResult.DismissMessage -> {
          previous.copy(message = null)
        }
      }
      state
    }
  }

  private fun compose(): Observable<TopStarsState> {
    return intents
      // Take user intent and convert it to TopStarsResult
      .compose(intentProcessor())
      // Use previous state and a reducer to produce new state
      .scan(TopStarsState.EMPTY, reducer())
      .replayingShare(TopStarsState.EMPTY)
      .distinctUntilChanged()
  }
}