package com.alex.github.ui.repos

import com.alex.api.GithubApi
import com.alex.api.Repo
import com.alex.api.networking.Either
import com.alex.api.networking.NetworkError
import com.alex.api.networking.NetworkSingle
import io.reactivex.rxjava3.core.Single

data class FakeGithubApi(
  private val results: List<NetworkSingle<List<Repo>>>
) : GithubApi {
  private var called = 0

  override fun fetchTopStarredRepos(): NetworkSingle<List<Repo>> {
    val response = results[called]
    called += 1
    return response
  }

  companion object {
    fun failure(): NetworkSingle<List<Repo>> {
      return Single.just(Either.failure(NetworkError))
    }

    fun success(): NetworkSingle<List<Repo>> {
      return Single.just(
        Either.success(
          listOf(
            Repo(
              ownerAvatarUrl = "someUrl",
              owner = "owner",
              repo = "repo",
              topCommitter = "James"
            ),
            Repo(
              ownerAvatarUrl = "middleChildUrl",
              owner = "owner",
              repo = "repo",
              topCommitter = null
            ),
            Repo(
              ownerAvatarUrl = "jdUrl",
              owner = "JD",
              repo = "deanslist",
              topCommitter = "Dean"
            )
          )
        )
      )
    }
  }
}