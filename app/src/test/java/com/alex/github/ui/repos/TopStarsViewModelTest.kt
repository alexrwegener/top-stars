package com.alex.github.ui.repos

import com.alex.api.Repo
import com.alex.api.networking.NetworkSingle
import com.alex.github.ui.repos.FakeGithubApi.Companion.failure
import com.alex.github.ui.repos.FakeGithubApi.Companion.success
import com.google.common.truth.Truth.assertThat
import org.junit.Test

/**
 * Not an exhaustive lists of test but meant to showcase the testability of the view model.
 *
 * Given more time, we could separate the intent of the test with how the view model is constructed
 * Something like https://www.youtube.com/watch?v=7Y3qIIEyP5c
 */
class TopStarsViewModelTest {
  @Test
  fun `can load repos`() {
    val viewModel = createViewModel(success())

    val state = viewModel.state().test()

    viewModel.init()

    val values = state.assertNoErrors().assertNotComplete().values()

    assertThat(values).hasSize(3)
    assertThat(values[1].refreshing).isTrue()
    assertThat(values[2].repos).isNotEmpty()
  }

  @Test
  fun `can show error on initial load`() {
    val viewModel = createViewModel(failure())

    val state = viewModel.state().test()

    viewModel.init()

    val values = state.assertNoErrors().assertNotComplete().values()

    assertThat(values).hasSize(3)
    assertThat(values[1].refreshing).isTrue()
    assertThat(values[2].repos).isEmpty()
    assertThat(values[2].message).isNotNull()
  }

  @Test
  fun `can refresh after failure`() {
    val viewModel = createViewModel(failure(), success())

    val state = viewModel.state().test()

    viewModel.init()
    viewModel.pullToRefresh()

    val values = state.assertNoErrors().assertNotComplete().values()

    // Init, Loading, Error, Loading, Success
    assertThat(values).hasSize(5)
    assertThat(values[1].refreshing).isTrue()
    assertThat(values[2].repos).isEmpty()
    assertThat(values[2].message).isNotNull()
    assertThat(values[3].refreshing).isTrue()
    assertThat(values[4].repos).isNotEmpty()
    assertThat(values[4].message).isNull()
  }

  @Test
  fun `can dismiss snackbar`() {
    val viewModel = createViewModel(failure())

    val state = viewModel.state().test()

    viewModel.init()
    viewModel.dismissSnackbar()

    val values = state.assertNoErrors().assertNotComplete().values()

    // Init, Loading, Error, Dismiss
    assertThat(values).hasSize(4)
    assertThat(values[1].refreshing).isTrue()
    assertThat(values[2].repos).isEmpty()
    assertThat(values[2].message).isNotNull()
    assertThat(values[3].message).isNull()
  }

  private fun createViewModel(
    vararg responses: NetworkSingle<List<Repo>>
  ): TopStarsViewModel {
    return TopStarsViewModel(
      api = FakeGithubApi(results = responses.asList()),
      schedulers = ImmediateSchedulers()
    )
  }
}